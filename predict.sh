#!/bin/bash

# Config
DIR_BASE="/mnt/Damocles/master"
DATASTORE=${DIR_BASE}/datastore
NER=${DIR_BASE}/english.all.3class.distsim.crf.ser.gz
LANG_PROFILE=${DIR_BASE}/profiles
PID_FILE="predict.pid"
API_PATH="http://localhost:9090/core-rest/api"
HOST_IP="localhost"
GRADLE="./gradlew"
WAIT=15

# Do not modify
# Unless you know what you are doing !
# Otherwise, contact vincent.pasquier@gmail.com

# Terminal coloring
NORMAL=$(tput sgr0)
GREEN=$(tput setaf 2; tput bold)
YELLOW=$(tput setaf 3)
RED=$(tput setaf 1)

# Error
function red() {
    echo -e "$RED$*$NORMAL"
}

# Success
function green() {
    echo -e "$GREEN$*$NORMAL"
}

# Warning
function yellow() {
    echo -e "$YELLOW$*$NORMAL"
}

# Start
start () {
	if [[ ! -f ${PID_FILE} ]]; then
		rm -rf "*.log";
		green "PREDIct: Predict Conference Topics."
		green "Starting server..."
		nohup ${GRADLE} jettyRunWar -Dpredict.core.datastore=${DATASTORE} > log_server.log 2>&1 &
	else
		red "Server is already running."
	fi
}

# Stop
stop () {
	if [[ -f ${PID_FILE} ]]; then
		for pid in $(cat ${PID_FILE}); do
			kill ${pid};
		done
		rm -f ${PID_FILE};
	else
		red "The server is not running."
	fi
}

# Compile
compile () {
    green "Compiling client-rest-api...";
    ${GRADLE} client-rest-api:build -x test > /dev/null 2>&1;
    green "Compiling commons...";
    ${GRADLE} commons:build > /dev/null 2>&1;
    green "Compiling core-cache...";
    ${GRADLE} core-cache:build -x test > /dev/null 2>&1;
    green "Compiling core-entities...";
    ${GRADLE} core-entities:build > /dev/null 2>&1;
    green "Compiling core-reflect...";
    ${GRADLE} core-reflect:build > /dev/null 2>&1;
    green "Compiling core-rest...";
    ${GRADLE} core-rest:build -x test > /dev/null 2>&1;
    green "Compiling plugin-abstracts...";
    ${GRADLE} plugin-abstracts:build -x test > /dev/null 2>&1;
    green "Compiling plugins-conferences...";
    ${GRADLE} plugin-conferences:build -x test > /dev/null 2>&1;
    green "Compiling plugin-dblp...";
    ${GRADLE} plugin-dblp:build -x test > /dev/null 2>&1;
    green "Compiling plugin-microsoft-ranking...";
    ${GRADLE} plugin-microsoft-ranking:build -x test > /dev/null 2>&1;
    green "Compiling plugin-topics...";
    ${GRADLE} plugin-topics:build -x test > /dev/null 2>&1;
    green "Compiling plugin-wikicfp...";
    ${GRADLE} plugin-wikicfp:build -x test > /dev/null 2>&1;
    green "Compiling websockets-commons...";
    ${GRADLE} websockets-commons:build -x test > /dev/null 2>&1;
    green "Compiling websockets-plugins...";
    ${GRADLE} websockets-plugins:build -x test > /dev/null 2>&1;
    green "Compiling websockets-server...";
    ${GRADLE} websockets-server:build -x test > /dev/null 2>&1;
}

# Status
status () {
	if [[ -f ${PID_FILE} ]]; then
		green "The server is running."
	else
		yellow "The server is not running."
	fi
}

### main logic ###
case "$1" in
  start)
		start
		;;
  stop)
		stop
		;;
  status)
		status
		;;
  restart|reload|condrestart)
		stop
		start
		;;
  compile)
        compile
        ;;
  *)
		yellow "Usage: $0 {start|stop|compile|restart|reload|status}"
		exit 1
esac
exit 0