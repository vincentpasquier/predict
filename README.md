# PREDIct

Predict Conference Topic is a system built for my Master degree at [HES-SO](http://www.hes-so.ch/) in Computer Science.

Start by cloning repository then clone each submodule:

```
#!bash
git clone https://vincentpasquier@bitbucket.org/vincentpasquier/predict.git
git submodule init
git submodule update
```